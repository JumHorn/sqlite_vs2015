cmake_minimum_required(VERSION 3.0)

project(sqlite3)

set(CMAKE_INCLUDE_CURRENT_DIR ON)
set(CMAKE_ARCHIVE_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_LIBRARY_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)
set(CMAKE_RUNTIME_OUTPUT_DIRECTORY ${CMAKE_BINARY_DIR}/bin)

# add_definitions(-Wno-deprecated-declarations)
add_definitions(-DSQLITE_CORE)
# add_definitions(-DSQLITE_ENABLE_RTREE)
# add_definitions(-DSQLITE_ENABLE_COLUMN_METADATA)

FILE(GLOB SRC_FILES "./src/*.c")
FILE(GLOB HEAD_FILES "./src/*.h")

add_executable(sqlite3 ${SRC_FILES} ${HEAD_FILES})
target_link_libraries(sqlite3 dl pthread tcl)
